//
//  AnimationViewController.swift
//  ios71ls8
//
//  Created by WA on 6/12/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class AnimationViewController: UIViewController {

    @IBOutlet weak var movableViwe: UIView!
    
    @IBAction func panAction(_ sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .ended:
            UIView.animate(withDuration: 0.1) {
                let value = CGFloat(Int.random(in: 10...50))
                self.movableViwe.backgroundColor = .random()
                print(value)
                self.movableViwe.layer.cornerRadius = value
            }
        case .changed:
             movableViwe.center = sender.location(in: view)
        default:
            break
        }
    }
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static func random() -> UIColor {
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
}
