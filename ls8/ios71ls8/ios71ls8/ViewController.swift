//
//  ViewController.swift
//  ios71ls8
//
//  Created by WA on 6/12/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    private let data = ["Artem", "Anton", "Valera", "Timur", "NIkita"]
    private let images = [UIImage(named: "cat1"), UIImage(named: "cat1"), UIImage(named: "cat3"), UIImage(named: "cat2"), UIImage(named: "cat2")]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "CellTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "cellTable")

        let header = UIView()
        header.backgroundColor = .black
        header.frame = CGRect(x: 0, y: 0, width: 375, height: 50)
        tableView.tableHeaderView = header

        let footer = UIView()
        footer.frame = CGRect(x: 0, y: 0, width: 375, height: 50)
        footer.backgroundColor = .green
        tableView.tableFooterView = footer
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
//        cell.textLabel?.text = data[indexPath.row]
//        cell.imageView?.image = images[indexPath.row]

        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cellTable", for: indexPath) as? CellTableViewCell else { return UITableViewCell() }

        cell.cellImageView.image = images[indexPath.row]

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
}

class Volk: Animal, Hishnic {
    var ostrieToothes: Bool = true

    var pancir: Bool = false
}

class Animal {
    
}

protocol Hishnic {
    var ostrieToothes: Bool { get set }
    var pancir: Bool { get set }
}
