//
//  DictionatyViewController.swift
//  ios71ls8
//
//  Created by WA on 6/12/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class DictionatyViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    private var dictionary: [String: [String]] = [
        "A": ["Artem", "Anton"],
        "B": ["Bidon"],
        "C": ["Carton", "Cat"]
    ]

    private var sectionHeader: [String] { return Array(dictionary.keys).sorted(by: {$0 < $1}) }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
//        tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .middle)
    }
}

extension DictionatyViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dictionary.keys.count
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionHeader[section]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 100 : 50
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 1 ? 100 : 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print(indexPath)
        print(dictionary[sectionHeader[indexPath.section]]?[indexPath.row])
        let cell = tableView.cellForRow(at: indexPath)
        cell?.textLabel?.text = "DON'T TOUCH ME"
//        cell?.setSelected(false, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let key = sectionHeader[section]
        return dictionary[key]?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)

        cell.textLabel?.text = dictionary[sectionHeader[indexPath.section]]?[indexPath.row]
        return cell
        
    }
}
