//
//  UIColor+Extension.swift
//  ios68ls6
//
//  Created by WA on 6/5/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

extension UIColor {
    static var atomicTangerine: UIColor { return UIColor(red:1.00, green:0.60, blue:0.40, alpha:1.0) }
//    static var greenGreen
}
