//
//  ViewController.swift
//  ios68ls6
//
//  Created by WA on 6/5/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var statusLabel: UILabel!

    var isDaySetting: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "isDaySetting")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "isDaySetting")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationController?.navigationBar.topItem?.title = "My first VC"
//        navigationController?.navigationBar.topItem?.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addSomeStuff))
        NotificationCenter.default.addObserver(self, selector: #selector(observeOrientation), name: UIDevice.orientationDidChangeNotification, object: nil)

        // PLIST
//        saveArrayOfString(["Artem", "Anton", "Illya"])
//        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
//        readFromFile(documentDirectory.appendingPathComponent("testFile.plist"))

        let date = Date()
        print(date.timeIntervalSince1970)
//        let data = Data()
        
        // USER DEFAULTS
//        UserDefaults.standard.set("SomeValue", forKey: "SomeKey")
//        isDaySetting = true
//        print(isDaySetting)

        // UICOLOR
//        let color = UIColor(rgb: 0xFFFFFFFF)
        view.backgroundColor = Robot.color
//        Robot.foo()

        // UIFONT
        statusLabel.font = .regular13

        // NSAttributedString
//        statusLabel.attributedText = NSAttributedString(string: "Text")

        let attributedText = NSMutableAttributedString(string: "Attributed")
        let nsrange = NSRange(location: 0, length: "Attributed".count)
        attributedText.addAttribute(
            .underlineStyle,
            value: NSUnderlineStyle.single.rawValue,
            range: nsrange)
//        attributedText.addAttributes(NSAttributedString.Key. : NSAttribut, range: <#T##NSRange#>)
        
//        statusLabel.attributedText = attributedText
        
        
        // TEXTVIEW
//        let textView = UITextView()

        // scrollview
        let scrollView = UIScrollView()
//        scrollView.zoomScale
//        scrollView.scrollsToTop
        scrollView.contentInset = UIEdgeInsets(top: -50, left: -50, bottom: -50, right: -50)

        // shadow
        statusLabel.shadowColor = .black
        statusLabel.shadowOffset = CGSize(width: 1, height: 1)
        
        statusLabel.layer.cornerRadius = statusLabel.frame.height / 2
        statusLabel.backgroundColor = .black
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let vc = segue.destination as? DetailViewController {
            vc.delegate = self
        }
    }

    func saveArrayOfString(_ array: [String]) {
        let nsArray = array as NSArray
        do {
            let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            try nsArray.write(to: documentDirectory.appendingPathComponent("testFile.plist"))
        } catch {
            print(error.localizedDescription)
        }
    }

    func readFromFile(_ url: URL) {
//        let nsArray = NSArray(contentsOf: url)
//        print(nsArray)
    }
    
    @objc func addSomeStuff() { }

    @objc func observeOrientation() {
        print("Orientation did change")
        print(statusLabel.someProp)

        let someButton = UIButton()
        print(someButton.someProp)
        print(someButton.randomNotTen)
    }

    @IBAction func doStuffFromVC(_ sender: UIBarButtonItem) {
        print("ALIVE")
    }
}

extension ViewController: DetailViewControllerDelegate {
    func userDidPressButton() {
        statusLabel.text = "USER TAP BUTTON"
    }
}

extension UIView {
    var someProp: Int { return Int.random(in: 0...5) }
}

extension UIButton {
    var randomNotTen: Int { return Int.random(in: 10...15) }
}

class Robot {
    var health: Int
    var damage: Int
    static var color = UIColor.atomicTangerine

    static func foo() { }

    init(health: Int, damage: Int) {
        self.health = health
        self.damage = damage
    }
}
