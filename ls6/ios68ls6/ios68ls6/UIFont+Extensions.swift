//
//  UIFont+Extensions.swift
//  ios68ls6
//
//  Created by WA on 6/5/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

extension UIFont {
    static var regular13: UIFont {
        return UIFont(name: "Helvetica", size: 13)!
    }
}
