//
//  DetailViewController.swift
//  ios68ls6
//
//  Created by WA on 6/5/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

protocol DetailViewControllerDelegate {
    func userDidPressButton()
}

class DetailViewController: UIViewController {

    var delegate: DetailViewControllerDelegate?

    @IBAction func didPressUseButton(_ sender: UIButton) {
//        if delegate is ViewController {
//            print("I am ViewController")
//        }
        delegate?.userDidPressButton()
    }
}
