//
//  ViewController.swift
//  ios71ls9
//
//  Created by WA on 6/17/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private var counter: CGFloat = 1
    private var offset: CGFloat = 10
    private var timer: Timer?
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var progressWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var secondProgressView: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        view.layer.cornerRadius
//        view.layer.borderWidth
//        loadData()
//        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(fire), userInfo: nil, repeats: true)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print(view.safeAreaInsets)
    }

    @objc func fire()  {
        guard counter * offset <= 100 else {
            secondProgressView.progress = 1
//            progressWidthConstraint.constant = 375
            timer?.invalidate()
            return
        }
        secondProgressView.progress = Float(counter * offset / 100)
//        progressWidthConstraint.constant = counter * offset
        counter += 1
        print(counter)
    }
}
