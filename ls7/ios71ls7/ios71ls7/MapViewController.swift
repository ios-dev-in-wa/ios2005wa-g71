//
//  MapViewController.swift
//  ios71ls7
//
//  Created by WA on 6/10/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class MapViewController: UIViewController {
    let location = "Kiev"

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.topItem?.leftBarButtonItem = UIBarButtonItem(title: "X", style: .plain, target: self, action: #selector(close))
//        navigationController?.navigationBar.topItem?.leftBarButtonItem?.tintColor = .black
    }

    @objc private func close() {
        // 1 type remove VC
//        delayedClose()
        // 2 type
//        navigationController?.dismiss(animated: true, completion: nil)
        // 3 type
//        navigationController?.popViewController(animated: true)
        

        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
            self?.delayedClose()
        }
    }

    @objc func delayedClose() {
       self.dismiss(animated: true, completion: nil)
    }
}
