//
//  PigPieViewController.swift
//  ios71ls7
//
//  Created by WA on 6/10/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class PigPieViewController: UIViewController {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

        guard let image = UIImage(named: "pigPie") else { return }
        view.backgroundColor = UIColor(patternImage: image)
    }
    override func loadView() {
        super.loadView()
        print("PigPie Load view called")
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
//        if #available(iOS 11.0, *) {
//            // Running iOS 11 OR NEWER
//        } else {
//            // Earlier version of iOS
//        }
        print("PigPie viewDidLoad called")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("PigPie viewWillAppear called")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("PigPie viewDidAppear called")
        if Settings.shared.isSoundOn {
            
        }
//        Settings.saveSetting(value: <#T##Any?#>, key: <#T##String#>)
    }

}

class Robot {

    static let shared = Robot()

    let hitPoints: Int = 100
}

class Settings {

    static let shared = Settings()

    var isSoundOn: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "isSoundOn")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "isSoundOn")
        }
    }

    static func saveSetting(value: Any?, key: String) {
        UserDefaults.standard.set(value, forKey: key)
    }
}
