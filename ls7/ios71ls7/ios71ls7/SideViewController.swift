//
//  SideViewController.swift
//  ios71ls7
//
//  Created by WA on 6/10/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class SideViewController: UIViewController {

    var imageName: String = "0"
    var image: UIImage?

    @IBOutlet var rightSwipeGesture: UISwipeGestureRecognizer!
    @IBOutlet weak var imageVIew: UIImageView!

    override func loadView() {
        super.loadView()
        print("Load view called")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad called")
//        if imageVIew == nil {
//            let box = UIView()
//            box.backgroundColor = .red
//            box.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
//            view.addSubview(box)
//        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        imageVIew.image = UIImage(named: imageName)
        print("viewWillAppear called")
        if imageVIew != nil {
            imageVIew.image = image
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear called")
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        navigationController?.popViewController(animated: true)
    }

    @IBAction func panGestureAction(_ sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .possible:
            print("Possible")
        case .began:
            print("Began")
        case .changed:
            print(sender.location(in: view))
//            print("Changed")
        case .ended:
            print("Ended")
        case .cancelled:
            print("cancelled")
        case .failed:
            print("Failed")
        @unknown default:
            break
        }
    }

    @IBAction func didSwipeRight(_ sender: UISwipeGestureRecognizer) {
        
        print("Gesture enabled")
    }
}
