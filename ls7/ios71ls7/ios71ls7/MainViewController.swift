//
//  ViewController.swift
//  ios71ls7
//
//  Created by WA on 6/10/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBAction func didPressButton(_ sender: UIButton) {
        let imageName = String(sender.tag)

        imageView.image = UIImage(named: imageName)
        performSegue(withIdentifier: "showSide", sender: imageName)
//        let viewC = SideViewController()

//        navigationController?.pushViewController(viewC, animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let sideVC = segue.destination as? SideViewController {
//            let sender = sender,
//            let imageName = sender as? String {
            sideVC.image = imageView.image
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
//        performSegue(withIdentifier: "showSide", sender: "2")
    }

    override func loadView() {
        super.loadView()
        print("Main Load view called")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Main viewDidLoad called")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("Main viewWillAppear called")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("Main viewDidAppear called")
    }
    @IBAction func didChooseSegment(_ sender: UISegmentedControl) {
//        sender.insertSegment(withTitle: "Test1", at: sender.numberOfSegments, animated: true)
        if sender.selectedSegmentIndex == 1 {
//            let story = UIStoryboard(name: "Story", bundle: Bundle.main)
//            story.instantiateViewController(withIdentifier: <#T##String#>)
            if let viewC = storyboard?.instantiateViewController(withIdentifier: "MapVC") as? MapViewController {
                print(viewC.location)
//                navigationController?.pushViewController(viewC, animated: true)
                let navC = UINavigationController(rootViewController: viewC)
                navC.modalTransitionStyle = .crossDissolve
                navigationController?.present(navC, animated: true, completion: {
                    print("Completed")
                })
            }
//            performSegue(withIdentifier: "showMap", sender: nil)
        }
    }

    @IBAction func didToggleSwitcg(_ sender: UISwitch) {
        view.backgroundColor = sender.isOn ? .black : .green
    }
}

