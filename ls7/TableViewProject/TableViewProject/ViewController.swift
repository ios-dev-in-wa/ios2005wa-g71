//
//  ViewController.swift
//  TableViewProject
//
//  Created by WA on 6/12/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit
import MapKit
import AMCalendar
import SPPermission

struct LocationItem {
    let currentSpeed: String
    let currentDate: Date
    let id: UUID
}

class ViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var tableView: UITableView!

    private let locationManager = CLLocationManager()

    var locations: [LocationItem] = []
//    var testLocations = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        mapView.showsUserLocation = true
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()

        tableView.register(UINib(nibName: "CustomCellTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "customCell")
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard !SPPermission.isAllowed(.camera) else { return }
        SPPermission.Dialog.request(with: [.camera], on: self)
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let calendar =
//            AMCalendarRootViewController.setCalendar(onView: view,
//                                                     parentViewController: self,
//                                                     selectedDate: Date(),
//                                                     delegate: self)
//        present(calendar, animated: true, completion: nil)
    }
}

extension ViewController: AMCalendarRootViewControllerDelegate {
    func calendarRootViewController(_ calendarRootViewController: AMCalendarRootViewController, didSelectDate date: Date?) {
        print(date)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locations.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "customCell", for: indexPath) as? CustomCellTableViewCell
        cell?.numberLabel.text = "\(indexPath.row + 1))"
        cell?.speedLabel.text = locations[indexPath.row].currentSpeed + "km/h"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss.SSS"
        let dateString = dateFormatter.string(from: locations[indexPath.row].currentDate)

        cell?.timeLabel.text = dateString
//        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
//        cell.textLabel?.text = locations[indexPath.row].currentSpeed + "km/h"
//
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "HH:mm:ss.SSS"
//        let dateString = dateFormatter.string(from: locations[indexPath.row].currentDate)
//
//        cell.detailTextLabel?.text = dateString
        return cell ?? UITableViewCell()
    }
}

extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let speedString = String(locations.first?.speed ?? -1)
        let locationItem = LocationItem(currentSpeed: speedString, currentDate: Date(), id: UUID())
        self.locations.insert(locationItem, at: 0)
//        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
//            self.locationManager.stopUpdatingLocation()
//        }

        tableView.reloadData()
    }
}
