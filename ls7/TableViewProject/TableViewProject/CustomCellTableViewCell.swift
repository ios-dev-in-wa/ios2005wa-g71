//
//  CustomCellTableViewCell.swift
//  TableViewProject
//
//  Created by WA on 6/17/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class CustomCellTableViewCell: UITableViewCell {

    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        numberLabel.text = nil
        speedLabel.text = nil
        timeLabel.text = nil
    }
}
