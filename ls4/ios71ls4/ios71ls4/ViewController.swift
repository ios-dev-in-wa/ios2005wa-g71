//
//  ViewController.swift
//  ios71
//
//  Created by Maryna Boronylo on 5/22/19.
//  Copyright © 2019 Maryna Boronylo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let some = "some,  someTest"
        let validateString = some.replacingOccurrences(of: "  ", with: " ")
        print(some)
        printSeparator()
        print(validateString)

        let car = "Mazda"

        if car.hasPrefix("Ma") {
            print("\(car) has prefix Ma")
        }

        printSeparator("== String")
        let userName = "Killer1998"
        let newUserName = "killer1998"
        if userName.lowercased() == newUserName.lowercased() {
            print("NAME IS ALREADY EXIST!")
        }

        var newString = "Taburetka"
        newString += " Vlada"
        printSeparator()
        print(newString)

        newString.append(" Artema")
        printSeparator("COOL TITLE")
        print(newString)

        // Big string
        printSeparator("Big string")
        let bigString = """
//
//  ViewController.swift
//  ios71
//
//  Created by Maryna Boronylo on 5/22/19.
//  Copyright © 2019 Maryna Boronylo. All rights reserved.
//

"""
        print(bigString)
        // Array
        var students = ["Artem", "Dima", "Sergey", "Dimidrol", "Artem", "Dimentr"]
        let studentsMarks = [1, 2, 1, 2]
        let studentsSurnames: [String] = ["Ignatovich", "Lobanovich"]

        if students.isEmpty { }
        let index = 1

        guard index < students.count - 1 else {
            print("Index out of range")
            return
        }
        let name = students[index]
        printSeparator()
        print(name)

        students.append("Valera")
        print(students)

        printSeparator()
        students.insert("Ksuha", at: 0)
        print(students)

        // Cycles
//        for i in 0..<students.count {
//            print("Hello,", students[i])
//        }

        students.forEach({ print("Hello,", $0) })

        let filteredData = students.filter({ $0.hasPrefix("Di") || $0.hasPrefix("Ar") })
        _ = students.filter { value -> Bool in
            !value.isEmpty
        }
        printSeparator("Filtered data")
        print(filteredData)

        let sortedData = students.sorted(by: { $0 < $1 })
        _ = students.sorted { firstValue, secondValue -> Bool in
            firstValue == secondValue
        }
        printSeparator("Sorted array")
        print(sortedData)

        // first
//        if let firstValue = students.first(where: {$0 == "Artem"}) { }

        // remove
//        students.removeAll()

        // index
        let someIndex = students.firstIndex(of: "Artem")
//        students[someIndex]
        _ = students.firstIndex(where: { !$0.isEmpty })
        
        
        // Cool sort
        printSeparator("Cool sort")
        let test = students.filter({$0 == "Artem"})
        print(test)

        // Dictionary
        printSeparator("Dictionary")
        let dictinary: [String: String] = ["Artem": "Velykyy"]
        var phoneBook = [
            "Artem": 380632222222,
            "Valera": 380502222222,
            "Anna": 380972222222
        ]

        printSeparator("Dictionary [key: value]")
        if let surname = dictinary["Artem22222"] {
            print(surname)
        }

//        phoneBook.keys.forEach {
//            printSeparator($0)
//        }

        phoneBook["Artem"] = 101
        phoneBook.removeValue(forKey: "Artem")
        
        phoneBook.values.forEach {
            printSeparator(String($0))
        }
//
//        print(phoneBook.keys)
        
        // SET
        let someSet: Set<Int> = [1, 2, 3, 1]
        let secondSet: Set<Int> = [1, 3, 4]
        let intesection = someSet.intersection(secondSet)

        print(intesection)

        // Class creation
        printSeparator("Class creation")
        let blueCabryR18 = Car(bodyColor: .blue, bodyType: .cabry, wheelRadius: 18)
        let redSedanR16 = Car(bodyColor: .red, bodyType: .sedan, wheelRadius: 16)

        print("My car bodyType is", blueCabryR18.bodyType, "My friend's car bodyType is", redSedanR16.bodyType)
        
        // cool if usage
        printSeparator("cool if usage")
        let neeedToHeader = !view.isHidden && view.frame.width == 100 && view.backgroundColor == .red
        if neeedToHeader { }

        print("Finish exucution")

        // Electic car public/private
//        let tesla = ElectricCar(bodyColor: .red, bodyType: .cabry, wheelRadius: 19, voltage: 85.0)
//        tesla.changeCar(kW: 10)
//
//        tesla.turnRight()
//        print(tesla.bodyColor, tesla.wheelRadius)

        // UIViewController init
//        let customVC = UIViewController()

        // UIVIew init
//        let customView = CustomView()
        print(antimat("fuck you!!!!1111 BITCH, fucker, lucker"))
        print(anti("fuk, fuk , fuker"))
        
    }

    // Default title
    func printSeparator(_ title: String = "Default Title") {
        print("\n\n---------------\(title)---------------")
    }
    
    func antimat(_ str : String ) -> String {
        let badWords : Set<String> = ["fuck", "bitch"]
        var clearString = str
        
        for i in badWords {
            var replaceString = ""
            for _ in 0..<i.count {
                replaceString += "*"
            }
            clearString = clearString.lowercased().replacingOccurrences(of: i, with: replaceString)
        }
        
        return clearString
    }

    func anti(_ str: String) -> String{
        let antimat: Set<String> = ["fuck", "fuk"]
        var result = ""
        var splitString = str.split(separator: " ")
        for i in 0..<splitString.count {
            if antimat.contains(String(splitString[i])){
                splitString[i] = "***"
                result += splitString[i] + " "
            } else {
                result += splitString[i] + " "
            }
        }
        
        return result
    }
}

//class CustomView: UIView {
//    let count: Int
//
//    init(count: Int) {
//        super.init(frame: CGRect(x: 0, y: 0, width: 100, height: 200))
//        self.count = count
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        
//    }
//
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        
//    }
//}
