//
//  Car.swift
//  ios71ls4
//
//  Created by WA on 5/29/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class WheelVehicle {

    let wheelRadius: Int

    init(wheelRadius: Int) {
        self.wheelRadius = wheelRadius
    }

    convenience init(moneyOwned: Double) {
        self.init(wheelRadius: moneyOwned > 100.0 ? 19 : 16)
    }

    deinit { }
}

class Car: WheelVehicle {

    enum BodyType: String {
        case coope, sedan, cabry
    }

    let bodyColor: UIColor
    let bodyType: BodyType

    init(bodyColor: UIColor, bodyType: BodyType, wheelRadius: Int) {
        self.bodyColor = bodyColor
        self.bodyType = bodyType
        super.init(wheelRadius: wheelRadius)
    }

    func getWheelRadius() -> Int {
        return wheelRadius
        
    }

//    func enumInit() {
//        if let cased = BodyType.init(rawValue: "coope") {
//
//        }
//    }
}

//class ElectricCar: Car {
//    private let voltage: Double
//    private var batteryLevel: Double = 100
//    
//    func changeCar(kW: Double) {
//        if batteryLevel < 100 {
//            batteryLevel += kW
//        }
//    }
//
//    private func turnLeft() {
//        // turn Baranka left
//    }
//
//    func turnRight() {
//        
//    }
//
////    init(bodyColor: UIColor, bodyType: BodyType, wheelRadius: Int, voltage: Double) {
////        super.init(bodyColor: bodyColor, bodyType: bodyType, wheelRadius: wheelRadius)
////        self.voltage = voltage
////    }
//}
