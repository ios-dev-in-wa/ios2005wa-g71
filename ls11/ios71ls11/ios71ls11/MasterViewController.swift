//
//  MasterViewController.swift
//  ios71ls11
//
//  Created by WA on 6/24/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit
import Parse

struct Car {
    let name: String
    let price: Int
    let id: String
}

class MasterViewController: UITableViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    
    var detailViewController: DetailViewController? = nil
    var cars = [Car]()
    var isDatePickerNeeded = false

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let loginBarButton = UIBarButtonItem(title: "Login", style: .plain, target: self, action: #selector(showLogin))
        navigationItem.leftBarButtonItem = loginBarButton
//        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(insertNewObject(_:)))
//        navigationItem.rightBarButtonItem = addButton
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        tableView.register(UINib(nibName: "DatePickerCellTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "datePickerCell")
        searchBar.delegate = self
        loadData()
    }

    @objc func showLogin() {
        let loginVC = PFLogInViewController()
        present(loginVC, animated: true, completion: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadData()
    }

    private func loadData() {
        cars.removeAll()
        let query = PFQuery.init(className: "Car")
        query.findObjectsInBackground { [weak self] (objects, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            objects?.forEach {
                self?.cars.append(Car(name: $0["name"] as? String ?? "", price: $0["price"] as? Int ?? 0, id: $0.objectId ?? "12312312"))
                self?.defaultValue = self!.cars
            }
            self?.tableView.reloadData()
        }
    }

    @objc
    func insertNewObject(_ sender: Any) {
//        objects.removeAll()
//        for _ in 0...4 {
//            objects.append(Int.random(in: 0..<100))
//        }
        UIView.transition(with: view, duration: 0.5, options: [.transitionFlipFromLeft, .autoreverse, .repeat], animations: {
            self.tableView.reloadData()
        }, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
            self?.view.layer.removeAllAnimations()
        }
        
//        objects.insert(NSDate(), at: 0)
//        let indexPath = IndexPath(row: 0, section: 0)
//        tableView.insertRows(at: [indexPath], with: .automatic)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "showDetail" {
//            if let indexPath = tableView.indexPathForSelectedRow {
//                let object = cars[indexPath.row]
//                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
//                controller.detailItem = object
//                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
//                controller.navigationItem.leftItemsSupplementBackButton = true
//            }
//        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !isDatePickerNeeded, indexPath.row == cars.count - 1 {
            isDatePickerNeeded = true
            let lastItemIndexPath = IndexPath(row: indexPath.row + 1, section: indexPath.section)
            tableView.insertRows(at: [lastItemIndexPath], with: .automatic)
        } else if isDatePickerNeeded {
            isDatePickerNeeded = false
            let lastIndexPath = IndexPath(row: cars.count, section: indexPath.section)
            tableView.deleteRows(at: [lastIndexPath], with: .automatic)
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let inset = isDatePickerNeeded ? 1 : 0
        return cars.count + inset
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isDatePickerNeeded, let cell = tableView.dequeueReusableCell(withIdentifier: "datePickerCell") as? DatePickerCellTableViewCell
        {
            cell.delegate = self
            return  cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let object = cars[indexPath.row]
        cell.accessoryType = UITableViewCell.AccessoryType(rawValue: 3) ?? .none

        cell.textLabel!.text = object.name
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

//    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
//        return UITableViewCell.EditingStyle(rawValue: 3) ?? .none
//    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let query = PFQuery(className: "Car")
            query.getObjectInBackground(withId: cars[indexPath.row].id) { objects, error in
                objects?.deleteInBackground()
            }
            cars.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }

 var defaultValue = [Car]()
}

extension MasterViewController: DatePickerCellTableViewCellDelegate {
    func didPickDate(_ date: Date) {
        print(date)
    }    
}

extension MasterViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchedText = searchBar.text else { return }
        
        cars = cars.filter { $0.name.lowercased().contains(searchedText.lowercased()) }
        tableView.reloadData()
    }
}
