//
//  DetailViewController.swift
//  ios71ls11
//
//  Created by WA on 6/24/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit
import AVKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!


    func configureView() {
        // Update the user interface for the detail item.
        if let car = detailItem {
            if let label = detailDescriptionLabel {
                label.text = car.name
            }
        }
//        let impact = UIImpactFeedbackGenerator(style: .light)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()

        let url = Bundle.main.bundleURL.appendingPathComponent("sirena.mp3")
        do {
            let player = try AVAudioPlayer(contentsOf: url)
            player.play()
        } catch {
            print(error.localizedDescription)
        }
    }

    var detailItem: Car? {
        didSet {
            // Update the view.
            configureView()
        }
    }


}

