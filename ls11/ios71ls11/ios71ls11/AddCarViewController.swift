//
//  AddCarViewController.swift
//  ios71ls11
//
//  Created by WA on 6/24/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit
import Parse

class AddCarViewController: UITableViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var priceTextField: UITextField!

    @IBAction func cancelAction(_ sender: UIBarButtonItem) {
        navigationController?.dismiss(animated: true, completion: nil)
    }

    @IBAction func saveAction(_ sender: UIBarButtonItem) {
        guard let text = priceTextField.text, !text.isEmpty else { return }
        let parseObject = PFObject(className:"Car")
        
        parseObject["name"] = nameTextField.text
        parseObject["price"] = Int(priceTextField.text ?? "0")
        parseObject["owner"] = PFUser.current()
        
        // Saves the new object.
        print(parseObject.allKeys)

        parseObject.saveInBackground {
            (success: Bool, error: Error?) in
            if success {
                print("SUPER SAVE")
            } else {
                print(error?.localizedDescription)
            }
        }
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    
}
