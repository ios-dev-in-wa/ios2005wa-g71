//
//  AuthManager.swift
//  ios71ls12
//
//  Created by WA on 6/26/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

class AuthManager {

    static let shared = AuthManager()

    private let userDefaults = UserDefaults.standard

    var userToken: String? {
        get {
            return userDefaults.string(forKey: "userToken")
        }
        set {
            userDefaults.set(newValue, forKey: "userToken")
        }
    }
}
