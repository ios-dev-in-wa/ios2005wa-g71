//
//  Networking.swift
//  ios71ls12
//
//  Created by WA on 6/26/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

struct LoginRequest: Codable {
    let name: String
    let password: String
}

struct CarResponse: Codable {
    let results: [Car]
}

struct Car: Codable {
    let name: String
    let price: Int
}

struct CarSession {
    
    private let session = URLSession(configuration: .default)

    func loginWith(loginRequest: LoginRequest) {
        
    }
    
    func getCar(completion: (([Car]) -> Void)?) {
        guard let url = URL(string: "https://parseapi.back4app.com/classes/Car") else { return }
        var request = URLRequest(url: url)
        
        request.addValue("E8pTahibFq4mRfotZlldvzLtFrHpUbodT5Nt9xj1", forHTTPHeaderField: "X-Parse-Application-Id")
        request.addValue("ArbWajewwPWQfVGS8Bxues1sXI1XdFo8tQTrMCEx", forHTTPHeaderField: "X-Parse-REST-API-Key")
        
        request.httpMethod = "GET"
        
        session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
//            var json = [String: Any]()
//            do {
//                json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? [String: Any] ?? [:]
//            } catch {
//                print(error)
//            }
//
//            print(json["results"])
//            return

            if let jsonData = data,
                let response = response as? HTTPURLResponse,
                response.statusCode == 200 {
                switch response.statusCode {
                case 404:
                    print("404 error")
                case 200:
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .iso8601
                    do {
                        let decoded = try decoder.decode(CarResponse.self, from: jsonData)
                        completion?(decoded.results)
                    } catch {
                        print("Problem parsing JSON: \(error)")
                    }
                default:
                    print("UNRECOGNIZED ERROR")
                }
            }
            }
            .resume()
    }
}
