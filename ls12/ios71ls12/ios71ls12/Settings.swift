//
//  Singleton.swift
//  ios71ls12
//
//  Created by WA on 6/26/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

class Settings {
    
    static let shared = Settings()

    private let userDefaults = UserDefaults.standard

    var soundOn: Bool {
        get {
            return userDefaults.bool(forKey: "soundOn")
        }
        set {
            userDefaults.set(newValue, forKey: "soundOn")
        }
    }

    var isLoggedIn: Bool {
        get {
            return userDefaults.bool(forKey: "isLoggedIn")
        }
        set {
            userDefaults.set(newValue, forKey: "isLoggedIn")
        }
    }

    var brightLebel = 1
    var isScreenLocked = false
}
