//
//  ViewManager.swift
//  ios71ls12
//
//  Created by WA on 6/26/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ViewManager {

    static let shared = ViewManager()

    private let appDelegate = UIApplication.shared.delegate as? AppDelegate

    private let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    private let loginStoryboard = UIStoryboard(name: "Login", bundle: Bundle.main)

    func setupInitialController() {
        if let window = self.appDelegate?.window {
            let rootVC = loginStoryboard.instantiateInitialViewController()
            let mainVC = mainStoryboard.instantiateInitialViewController()
            window.rootViewController = Settings.shared.isLoggedIn ? mainVC : rootVC
        }
    }
}
