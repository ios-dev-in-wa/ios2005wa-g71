//
//  MainViewController.swift
//  ios68ls3
//
//  Created by WA on 5/27/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    enum Arithmetic: String {
        case plus = "+"
        case minus = "-"
        case divide = "/"
        case multiply = "*"
        case none = ""
    }

    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var plusButton: UIButton!

    var lastAction: Arithmetic = .none
    var firstNumber: String = ""
    var secondNumber: String?
    var displayedValue: String = ""
    var ariphemeticDidEnd: Bool = false

    @IBAction func didPressArithmeticAction(_ sender: UIButton) {
        ariphemeticDidEnd = false
        switch sender.title(for: .normal) {
        case "+":
             print("Action plus")
            lastAction = .plus
        case "-":
            print("Action minus")
            lastAction = .minus
        default:
            print("Default")
        }
    }

    @IBAction func didPressEqual(_ sender: UIButton) {
        if let secondNumber = secondNumber  {
            var result = 0
            let first = Int(firstNumber) ?? 0
            let second = Int(secondNumber) ?? 0
            switch lastAction {
            case .plus:
                print("Equal Action plus")
                result = first + second
            case .minus:
                print("Equal Action minus")
                result = first - second
            default:
                break
            }
            displayedValue = String(result)
            resultLabel.text = displayedValue
        } else {
            displayedValue = ""
            resultLabel.text = displayedValue
        }
        lastAction = .none
        secondNumber = nil
        ariphemeticDidEnd = true
    }

    @IBAction func didPressNumber(_ sender: UIButton) {
        if let numberString = sender.title(for: .normal) {
            switch lastAction {
            case .minus, .divide, .multiply, .plus:
                displayedValue = secondNumber == nil ? numberString : displayedValue + numberString
                if let nonOptionalNumber = secondNumber {
                    secondNumber = nonOptionalNumber + numberString
                } else {
                    secondNumber = numberString
                }
                resultLabel.text = displayedValue
            case .none:
                firstNumber = ariphemeticDidEnd ? numberString : firstNumber + numberString

                displayedValue = firstNumber
                resultLabel.text = displayedValue
            }
        }
    }

    @IBAction func didPressClear(_ sender: UIButton) {
        displayedValue = ""
        resultLabel.text = displayedValue
    }
}
