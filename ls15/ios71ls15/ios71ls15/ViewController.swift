//
//  ViewController.swift
//  ios71ls15
//
//  Created by WA on 7/8/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

struct Response: Codable {
    let results: [User]
}

struct User: Codable {
    let name: String
    let gender: String
}

class ViewController: UIViewController {
    
    @IBOutlet weak var helloLabel: UILabel!
    private var users: [User] = []

    @IBOutlet weak var howAreYouLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadData()
        helloLabel.text = "mainHelloLabel".localized
        howAreYouLabel.text = "mainHowAreYouLabel".localized
//        print(Locale.current.regionCode)
//        print(Locale.current.languageCode)
        
        
    }

    func loadData() {
        guard let fileURL = Bundle.main.url(forResource: "starWarsJSON", withExtension: nil) else { return }
        guard let data = try? Data(contentsOf: fileURL) else { return }
        let decoder = JSONDecoder()
        let result = try? decoder.decode(Response.self, from: data)
        print(result?.results)
    }
}

class SomeView: UIView {
    
    static func present() {
        // VARIANT 1
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            // topController should now be your topmost view controller
        }
        // VARIANT 2
        guard let window = UIApplication.shared.keyWindow else { return }
        let v = UIView(frame: window.bounds)
        window.addSubview(v);
        v.backgroundColor = UIColor.black
        let v2 = UIView(frame: CGRect(x: 50, y: 50, width: 100, height: 50))
        v2.backgroundColor = UIColor.white
        v.addSubview(v2)
    }
}
