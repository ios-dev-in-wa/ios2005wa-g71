//
//  ViewController.swift
//  test
//
//  Created by WA on 5/21/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addBox(times: 4)
    }

    func addBox(times: Int) {
        let inset: CGFloat = 44
        let space: CGFloat = 10
        for i in 0..<times {
            let step = times - i
            for item in 0..<step {
                let box = UIView()
                box.backgroundColor = UIColor.red.withAlphaComponent(0.1 * CGFloat(item))
                box.frame.size.width = inset
                box.frame.size.height = inset
                let xCord = (inset + space) * CGFloat(item + 1)
                box.frame.origin.x = xCord
                let yCord = (inset + space) * CGFloat(i + 1)
                box.frame.origin.y = yCord
                view.addSubview(box)
            }
        }
    }
}

