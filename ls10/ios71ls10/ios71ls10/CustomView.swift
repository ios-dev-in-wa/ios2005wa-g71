//
//  CustomView.swift
//  ios71ls10
//
//  Created by WA on 6/19/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class CustomView: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
        print("awakeFromNib")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    init(color: UIColor) {
        super.init(frame: .zero)
        backgroundColor = color
        contentMode = .redraw
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
//        let some: Float = 0.0
//        let point = CGPoint.zero
//        let rect = CGRect.zero
//        rect.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        
        backgroundColor = .blue
    }

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
//         Drawing code
        super.draw(rect)

        let path = UIBezierPath()
        path.move(to: CGPoint(x: 10, y: 10))
        path.addLine(to: CGPoint(x: 100, y: 100))
        path.addLine(to: CGPoint(x: 150, y: 100))
        path.addLine(to: CGPoint(x: 10, y: 10))

        path.lineWidth = 2

        UIColor.green.setFill()
        UIColor.red.setStroke()

        path.fill()
        path.stroke()
        path.close()
        
    }
}
