//
//  ViewController.swift
//  ios71ls10
//
//  Created by WA on 6/19/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit
import SPPermission

typealias OptionalCallback = (() -> Void)
typealias LolString = String

struct Card {
    let frontView: UIView
    let backView: UIView
}

class ViewController: UIViewController {

    @IBOutlet weak var faceView: FaceView!
    @IBOutlet weak var imageView: UIImageView!
    
    var handler: OptionalCallback?
    var userFinishLoginFlowCompletion: ((Bool) -> Void)?
    var card: Card?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let customView = CustomView(color: .black)
        let animation = {
            self.view.addSubview(customView)
            customView.frame.size = CGSize(width: 200, height: 200)
            customView.alpha = 0.5
            customView.center = self.view.center
        }
        UIView.animate(withDuration: 2, animations: animation)
        
        UIView.animate(withDuration: 0.5, delay: 2, options: [], animations: {
            customView.frame.size = CGSize(width: 100, height: 100)
            customView.setNeedsDisplay()
        }, completion: { value in
//            val
        })

        handler = { [weak self] in
            self?.imageView.image = UIImage()
            print("TESTING")
        }
        
        if let completion = handler {
            completion()
            print("working")
        }

        doSomeStuff {
            print("FINISH DOING STUFF")
        }

        let someArray = [1, 4, 6, 3, 6]

        let result = someArray.filter { value -> Bool in
            return value != 1
        }

        print(result)

        let redFace = FaceView(frame: CGRect(x: 20, y: 20, width: 100, height: 100))
        redFace.faceColor = .red
        let blueFace = FaceView(frame: CGRect(x: 20, y: 70, width: 200, height: 300))
        blueFace.faceColor = .blue
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapFace))
        blueFace.addGestureRecognizer(tapGesture)
        
        card = Card(frontView: redFace, backView: blueFace)
        
        view.addSubview(redFace)
        redFace.isHidden = true
        view.addSubview(blueFace)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        SPPermission.Dialog.request(with: [.camera], on: self)
    }

    @objc func didTapFace() {
        UIView.transition(with: card!.backView, duration: 0.5, options: [.transitionFlipFromLeft, .showHideTransitionViews], animations: {
//            self.card?.frontView.isHidden = false
        }, completion: nil)
//        UIView.transition(from: card!.backView, to: card!.frontView, duration: 0.5, options: [.transitionFlipFromLeft, .showHideTransitionViews], completion: nil)
    }

    @IBAction func didTap(_ sender: UITapGestureRecognizer) {
        faceView.happyLevel = -20
       
//        UIView.animate(withDuration: 0.5, delay: 0, options: .transitionFlipFromLeft, animations: {
//            self.faceView.alpha = 0.5
//        }, completion: nil)
//        UIView.animate(withDuration: 0.5) {
//            self.imageView.center = sender.location(in: self.view)
//        }
//        let location = sender.location(in: self.view)
//        let currentLocation = self.imageView.center
//        UIView.animate(withDuration: 0.5, delay: 2, options: [.curveEaseOut], animations: {
//            self.imageView.center = location
//        }, completion: { _ in
////            self.imageView.center = currentLocation
//        })
//        faceView.changeHappyFace(withHappLevel: 40, andColor: .blue)
    }

    func doSomeStuff(then: () -> Void) {
        print("DOING STUFF")
        then()
    }

    @IBAction func pinching(_ sender: UIPinchGestureRecognizer) {
//        imageView.frame.size = CGSize(width: 200 * sender.scale, height: 200 * sender.scale)
        
    }
}

