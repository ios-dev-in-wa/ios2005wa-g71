//
//  MainViewController.swift
//  iosg71-ls5
//
//  Created by WA on 6/3/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var tapCounterLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var topChangeColorButton: UIButton!
    @IBOutlet weak var bottomChangeColorButton: UIButton!
    
    var counter: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        tapCounterLabel.backgroundColor = UIColor(red: CGFloat(arc4random()) / CGFloat(UInt32.max),
                                                  green: CGFloat(arc4random()) / CGFloat(UInt32.max),
                                                  blue: CGFloat(arc4random()) / CGFloat(UInt32.max), alpha: 1)
        tapCounterLabel.font = .systemFont(ofSize: 18)
        tapCounterLabel.textColor = UIColor(red: CGFloat(arc4random()) / CGFloat(UInt32.max),
                                            green: CGFloat(arc4random()) / CGFloat(UInt32.max),
                                            blue: CGFloat(arc4random()) / CGFloat(UInt32.max), alpha: 1)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let image = UIImage(named: "plus")
        imageView.image = image
    }

    @IBAction func didPressChangeColor(_ sender: UIButton) {
        switch sender {
        case topChangeColorButton:
            view.backgroundColor = UIColor(red: CGFloat(arc4random()) / CGFloat(UInt32.max),
                                           green: CGFloat(arc4random()) / CGFloat(UInt32.max),
                                           blue: CGFloat(arc4random()) / CGFloat(UInt32.max), alpha: 1)
        case bottomChangeColorButton:
            view.backgroundColor = .black
        default:
            break
        }
        
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        counter += 1
        tapCounterLabel.text = "Tap count: (\(counter))"
    }
}
