//
//  BattleRobot.swift
//  iosg71-ls5
//
//  Created by WA on 6/3/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

class BattleRobot: NSObject {
    private(set) var health: Int {
        didSet {
            if health <= 0 {
                print("Robot \(name) is dead")
            }
        }
    }
    // FILEPRIVATE
    fileprivate var armor: Int
    var name: String

    var damage: Int {
        return Int.random(in: 1...10)
    }
    
    override var description: String {
        return "ROBOT \(name) with \(health) HP "
    }

    init(name: String, hitPoints: Int, armor: Int) {
        self.name = name
        self.health = hitPoints
        self.armor = armor
    }

    func getDamage(_ attacker: BattleRobot) {
        let damage = attacker.damage
        guard health > 0 else { return }
        guard armor > 0  else {
            health -= damage
            print("\(attacker.name) punched \(name) with: \(damage) on health, currentHealt: \(health)\n")
            return
        }
        armor -= damage
        print("\(attacker.name) penetrate \(name) with: \(damage) on armor\n")
    }
}
