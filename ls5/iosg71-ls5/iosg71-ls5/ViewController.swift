//
//  ViewController.swift
//  iosg71-ls5
//
//  Created by WA on 6/3/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let redRobot = BattleRobot(name: "RedMagicBot", hitPoints: Int.random(in: 1...100), armor: Int.random(in: 0...10))
    let blueRobot = BattleRobot(name: "BlueWarrior", hitPoints: Int.random(in: 1...100), armor: Int.random(in: 0...10))

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

//        print(redRobot.health)
//        redRobot.getDamage(100)
//        makeRobotsFight(firstFighter: redRobot, secondFighter: blueRobot)
//        print(redRobot)
//        let currentDayType: DayType = .sunset
        saveToFileManager()
        saveToUserDefaults()
    }

    enum DayType: String {
        case day, night, morning, sunset

        func changeAutomaticly() {
            
        }
        
        var time: Int {
            switch self {
            case .day:
                return 10
            case .night:
                return 2
            default:
                return 0
            }
        }

//        func reactionOnWarning() {
//            switch self {
//            case .day:
//                return 10
//            case .night:
//                return 2
//            default:
//                return 0
//            }
//        }
    }

    func makeRobotsFight(firstFighter: BattleRobot, secondFighter: BattleRobot) {
        while firstFighter.health > 0 || secondFighter.health > 0 {
            firstFighter.getDamage(secondFighter)
            secondFighter.getDamage(firstFighter)
        }
    }

    func saveToFileManager() {
        let students = ["Valera", "Artem", "Nikolay"]

        do {
            let data = try NSKeyedArchiver.archivedData(withRootObject: students, requiringSecureCoding: false)
            let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
//            BB23B077-0229-41A2-A735-3E1FF2DABAD4
             let fileUrl = url.appendingPathComponent(UUID().uuidString)
            try data.write(to: fileUrl)
            let savedData = try Data.init(contentsOf: fileUrl)
            let array = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(savedData) as? [String]
            print(array)
        } catch {
            print(error.localizedDescription)
        }
    }

    func saveToUserDefaults() {
        UserDefaults.standard.set("Artem", forKey: "Lecturer")
        if let lecturer = UserDefaults.standard.string(forKey: "Lecturer") {
            print(lecturer)
        }
    }
}

