//
//  CustomCollectionViewCell.swift
//  ios71ls13
//
//  Created by WA on 7/1/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!

    func setupWith(user: CollectionUser) {
        imageView.image = UIImage(named: user.imageName)
        titleLabel.text = user.name
        ageLabel.text = String(user.age)
    }
}
