//
//  UIView+AutoLayout.swift
//  ios71ls13
//
//  Created by WA on 7/1/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

extension UIView {
    
    var someValue: Int { return 10 }

    func bindTo(_ view: UIView, inset: UIEdgeInsets) {
//        view.safeAreaLayoutGuide
//        view.safeAreaInsets.bottom
        leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: inset.left).isActive = true
        trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -inset.right).isActive = true
        topAnchor.constraint(equalTo: view.topAnchor, constant: inset.top).isActive = true
        bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -inset.bottom).isActive = true
    }

    func bindTo(_ view: UIView) {
        leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }

    func centeredTo(_ view: UIView) {
        centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}
