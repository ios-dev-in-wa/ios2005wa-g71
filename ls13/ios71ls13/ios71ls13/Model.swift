//
//  Model.swift
//  ios71ls13
//
//  Created by WA on 7/1/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation
// MARK: - APIResponse
struct APIResponse: Codable {
    let results: [User]
    let info: Info
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.infoTask(with: url) { info, response, error in
//     if let info = info {
//       ...
//     }
//   }
//   task.resume()

// MARK: - Info
struct Info: Codable {
    let seed: String
    let results, page: Int
    let version: String
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.resultTask(with: url) { result, response, error in
//     if let result = result {
//       ...
//     }
//   }
//   task.resume()

// MARK: - Result
struct User: Codable {
    let gender: String
    let name: Name
    let location: Location
    let email: String
    let login: Login
    let dob: Dob
    let registered: Dob
    let phone: String
    let cell: String
    let id: ID
    let picture: Picture
    let nat: String
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.dobTask(with: url) { dob, response, error in
//     if let dob = dob {
//       ...
//     }
//   }
//   task.resume()

// MARK: - Dob
struct Dob: Codable {
    let date: Date
    let age: Int
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.iDTask(with: url) { iD, response, error in
//     if let iD = iD {
//       ...
//     }
//   }
//   task.resume()

// MARK: - ID
struct ID: Codable {
    let name, value: String
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.locationTask(with: url) { location, response, error in
//     if let location = location {
//       ...
//     }
//   }
//   task.resume()

// MARK: - Location
struct Location: Codable {
    let street, city, state, postcode: String
    let coordinates: Coordinates
    let timezone: Timezone
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.coordinatesTask(with: url) { coordinates, response, error in
//     if let coordinates = coordinates {
//       ...
//     }
//   }
//   task.resume()

// MARK: - Coordinates
struct Coordinates: Codable {
    let latitude, longitude: String
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.timezoneTask(with: url) { timezone, response, error in
//     if let timezone = timezone {
//       ...
//     }
//   }
//   task.resume()

// MARK: - Timezone
struct Timezone: Codable {
    let offset, timezoneDescription: String
    
    enum CodingKeys: String, CodingKey {
        case offset
        case timezoneDescription = "description"
    }
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.loginTask(with: url) { login, response, error in
//     if let login = login {
//       ...
//     }
//   }
//   task.resume()

// MARK: - Login
struct Login: Codable {
    let uuid, username, password, salt: String
    let md5, sha1, sha256: String
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.nameTask(with: url) { name, response, error in
//     if let name = name {
//       ...
//     }
//   }
//   task.resume()

// MARK: - Name
struct Name: Codable {
    let title, first, last: String
}

//
// To read values from URLs:
//
//   let task = URLSession.shared.pictureTask(with: url) { picture, response, error in
//     if let picture = picture {
//       ...
//     }
//   }
//   task.resume()

// MARK: - Picture
struct Picture: Codable {
    let large, medium, thumbnail: String
}

// MARK: - Helper functions for creating encoders and decoders

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}
