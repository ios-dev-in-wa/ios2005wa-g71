//
//  WebKitViewController.swift
//  ios71ls13
//
//  Created by WA on 7/1/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit
import WebKit

protocol WebKitControllerDelegate: class {
    func didRemovePopup()
}

class WebKitViewController: UIViewController {

//    @IBOutlet weak var webView: WKWebView!
    let someView = UIView()

    weak var delegate: WebKitControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
//        let webKitView = WKWebView()
//        webKitView.translatesAutoresizingMaskIntoConstraints = false
//        view.addSubview(webKitView)
//        webKitView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
//        webKitView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
//        webKitView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
//        webKitView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
//
//        let request = URLRequest(url: URL(string: "https://www.youtube.com/watch?v=uxHObvAVDBE")!)
//        webKitView.load(request)
        someView.backgroundColor = .black
        someView.translatesAutoresizingMaskIntoConstraints = false
//        someView.frame.size = CGSize(width: 200, height: 200)
        view.addSubview(someView)
        someView.bindTo(view, inset: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100))
//        someView.centeredTo(view)
    }
    
    @IBAction func didTap(_ sender: UITapGestureRecognizer) {
        let point = sender.location(in: view)

        if !someView.frame.contains(point) {
            someView.removeFromSuperview()
            delegate?.didRemovePopup()
        }
    }
}
