// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let aPIResponse = try? newJSONDecoder().decode(APIResponse.self, from: jsonData)

//
// To read values from URLs:
//
//   let task = URLSession.shared.aPIResponseTask(with: url) { aPIResponse, response, error in
//     if let aPIResponse = aPIResponse {
//       ...
//     }
//   }
//   task.resume()

import Foundation


struct UserService {
    
    private let session = URLSession(configuration: .default)

    func getUsers() -> [User] {
        guard let url = URL(string: "https://randomuser.me/api/") else { return [] }
        session.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let response = response as? HTTPURLResponse, let data = data else { return }
            switch response.statusCode {
            case 200:
                let decoder = JSONDecoder()
                do {
                    let users = try decoder.decode(APIResponse.self, from: data)
                    print("USERS", users)
                } catch {
                    print(error.localizedDescription)
                }
            default: break
            }
        }.resume()
        return []
    }
}
// MARK: - URLSession response handlers

extension URLSession {
    fileprivate func codableTask<T: Codable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completionHandler(nil, response, error)
                return
            }
            completionHandler(try? newJSONDecoder().decode(T.self, from: data), response, nil)
        }
    }
    
    func aPIResponseTask(with url: URL, completionHandler: @escaping (APIResponse?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
}
