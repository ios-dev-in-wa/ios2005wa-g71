//
//  ViewController.swift
//  ios71ls13
//
//  Created by WA on 7/1/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

struct CollectionUser {
    let name: String
    let imageName: String
    let age: Int
}

class ViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!

    private var users: [CollectionUser] = [
        CollectionUser(name: "Andrew", imageName: "human1", age: 99),
        CollectionUser(name: "Victor", imageName: "human2", age: 40),
        CollectionUser(name: "Dimon", imageName: "human3", age: 4)
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        collectionView.delegate = self
        collectionView.dataSource = self
//        UserService().getUsers()
        collectionView.register(UINib(nibName: "CustomCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "CustomCollectionViewCell")
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.estimatedItemSize = CGSize(width: 222, height: 222)
            layout.itemSize = CGSize(width: 222, height: 222)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let controller = segue.destination as? WebKitViewController {
            controller.delegate = self
        }
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return users.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCell", for: indexPath) as? CustomCollectionViewCell else { return UICollectionViewCell() }
        cell.setupWith(user: users[indexPath.row])
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.row > 1 else {
            let alert = UIAlertController(title: "Error", message: "This row is not available", preferredStyle: .actionSheet)
            if let alert = alert.popoverPresentationController {
//                guard let cell = collectionView.cellForItem(at: indexPath) as? CustomCollectionViewCell else { return }
                alert.barButtonItem = navigationController?.navigationBar.topItem?.rightBarButtonItem
//                alert.sourceView = cell.ageLabel
//                alert.sourceRect = cell.ageLabel.bounds
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { alert in
                print("Erorr shown")
            }
            alert.addAction(cancelAction)
            let doneAction = UIAlertAction(title: "Done", style: .destructive) { [weak self] _ in
                self?.users.remove(at: indexPath.row)
                collectionView.reloadData()
            }
            alert.addAction(doneAction)
            present(alert, animated: true, completion: nil)
            return
        }
        print("SUCCESS")
    }
}

extension ViewController: WebKitControllerDelegate {
    func didRemovePopup() {
        print("Did removePopup, doing some stuff")
    }
}

extension ViewController: UIViewControllerTransitioningDelegate {
    
}
