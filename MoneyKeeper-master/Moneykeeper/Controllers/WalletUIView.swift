//
//  walletUIVew.swift
//  Moneykeeper
//
//  Created by Sashko Shel on 6/23/19.
//  Copyright © 2019 Sashko Shel. All rights reserved.
//

import UIKit

enum Currency: String {
    case usd, uah, rub
}

struct WalletStruct {
    var balance: Double
    var currency: Currency
    
}

@IBDesignable class WalletUIView: UIView {

    private let label = UILabel()
    private var wallet: WalletStruct

    @IBInspectable
    var cornerRadius: CGFloat = 10 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }

    init(wallet: WalletStruct) {
        self.wallet = wallet
        super.init(frame: .zero)
        label.text = String(wallet.balance) + " " + wallet.currency.rawValue
        backgroundColor = .red
        addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


