//
//  WalletsViewController.swift
//  Moneykeeper
//
//  Created by Sashko Shel on 6/23/19.
//  Copyright © 2019 Sashko Shel. All rights reserved.
//

import UIKit

class WalletsViewController: UIViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackViewLeadingConstraint: NSLayoutConstraint!
    
    private var currentPage: CGFloat = 0 {
        didSet {
            pageControl.currentPage = Int(currentPage)
        }
    }
    private var delta: CGFloat = 0
    private var startFingerCoordinates: CGFloat = 0
    private var fingerCoordinates: CGFloat = 0
    private var walletsViews: [WalletUIView] = []
    private var wallets: [WalletStruct] = [
        WalletStruct(balance: 100, currency: .uah),
        WalletStruct(balance: 99, currency: .usd),
        WalletStruct(balance: 101, currency: .rub),
        WalletStruct(balance: 102, currency: .uah)
    ]
    private let animDuration = 0.5
    private let swipeDelta:CGFloat = 50

    @IBOutlet weak var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageControl.numberOfPages = wallets.count
        let views = wallets.compactMap { WalletUIView(wallet: $0) }
        views.forEach {
            self.stackView.addArrangedSubview($0)
        }
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        pageControl.numberOfPages = views.count

        widthConstraint.constant = UIScreen.main.bounds.width * CGFloat(views.count)
    }

    @IBAction func leftSwipeAction(_ sender: UISwipeGestureRecognizer) {
        guard Int(currentPage) < wallets.count, currentPage >= 0  else { return }
        currentPage += 1
        let value = currentPage * UIScreen.main.bounds.width
        stackViewLeadingConstraint.constant = -value
        view.layoutIfNeeded()
    }

    @IBAction func rightSwipeAction(_ sender: UISwipeGestureRecognizer) {
        guard currentPage > 0, Int(currentPage) <= wallets.count else { return }
        currentPage -= 1
        
        stackViewLeadingConstraint.constant = currentPage * UIScreen.main.bounds.width
        view.layoutIfNeeded()
    }
}

