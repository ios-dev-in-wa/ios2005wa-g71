//
//  ViewController.swift
//  ios71ls14
//
//  Created by WA on 7/3/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

struct User {
    let name: String
    let images: [UIImage]
}

class ViewController: UIViewController {

    private var currentPage = 1
    private let characters = ["", ""]
    let topTableView = UITableView()
    let bottomTableView = UITableView()
    let users = [User(name: "", images: [])]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        CharactersService().getCharacters(page: 10, pageSize: 10)
        
        // MULTITHREADING
//        DispatchQueue.main.async {
//
//        }
    }

    func loadMore() {
        currentPage += 1
        CharactersService().getCharacters(page: currentPage, pageSize: 10)
    }

    func getTopCell(indexPath: IndexPath) -> UITableViewCell {
    return UITableViewCell()
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return characters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if topTableView == tableView {
            return getTopCell(indexPath: indexPath)
        } else {
            return getTopCell(indexPath: indexPath)
        }
//        if (characters.count - 1) - 2 == indexPath.row {
//            loadMore()
//        }
//        return UITableViewCell()
    }
}

extension UIImageView {
    public func imageFromServerURL(urlString: String, completion: (() -> Void)?) {
//        UINib(nibName: "Main", bundle: Bundle.main)
        self.image = nil
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
//                print(error)
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.image = image
                completion?()
            })
            
        }).resume()
    }}
