//
//  StartViewController.swift
//  ios71ls14
//
//  Created by WA on 7/3/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {
    
    let imageView = UIImageView(image: UIImage(named: "launchScreenDog"))
    let activityIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        activityIndicator.startAnimating()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        imageView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(imageView)
        view.addSubview(activityIndicator)
        activityIndicator.center = view.center
        imageView.center = view.center
        imageView.layer.cornerRadius = imageView.frame.height / 2
        imageView.imageFromServerURL(urlString: "https://amp.businessinsider.com/images/592f4169b74af41b008b5977-1136-852.jpg") { [weak self] in
            self?.activityIndicator.stopAnimating()
        }
    }
}
