//
//  Character.swift
//  ios71ls14
//
//  Created by WA on 7/3/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

struct Character: Codable {
    let url: String
    let name: String
    let culture: String
    let aliases: [String]
}
