//
//  CharactersService.swift
//  ios71ls14
//
//  Created by WA on 7/3/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

struct CharactersService {

    private let api = "https://www.anapioficeandfire.com/api/characters"
    private let session = URLSession(configuration: .default)

    // https://www.anapioficeandfire.com/api/characters?page=1&pageSize=10
    func getCharacters(page: Int, pageSize: Int) {
        let urlString = api + "?page=\(page)&pageSize=\(pageSize)"
        guard let url = URL(string: urlString) else { return }
        session.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let data = data else { return }
            let decoder = JSONDecoder()
            do {
            let characters = try decoder.decode([Character].self, from: data)
                print(characters)
            } catch {
                print(error.localizedDescription)
            }
        }
        .resume()
    }
}
